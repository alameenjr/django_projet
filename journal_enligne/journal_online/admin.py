from django.contrib import admin


from . import models
# Register your models here.
from .models import Sujet, Entree


@admin.register(Sujet)
class SujetAdmin(admin.ModelAdmin):
    list_display = ('titre', 'date_creation')
    list_filter = ('date_creation',)


@admin.register(Entree)
class EntreeAdmin(admin.ModelAdmin):
    list_display = ('text', 'date_creation', 'get_sujet')

    @staticmethod
    def get_sujet(obj):
        return obj.sujet.titre
        get_sujet.short_description = 'sujet'

# admin.site.register(models.Sujet)
# admin.site.register(models.Entree)

