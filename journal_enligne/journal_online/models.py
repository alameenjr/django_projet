from django.db import models
from django.utils import timezone


# Create your models here.


class Sujet(models.Model):
    titre = models.CharField(max_length=255)
    date_creation = models.DateTimeField(default=timezone.now, verbose_name="Date creation")

    class Meta:
        verbose_name_plural = "sujets"

    def __str__(self):
        return self.titre


class Entree(models.Model):
    sujet = models.ForeignKey(Sujet, on_delete=models.CASCADE)
    text = models.TextField()
    date_creation = models.DateTimeField(default=timezone.now, verbose_name="Date creation")

    class Meta:
        verbose_name_plural = "entrees"

    def __str__(self):
        return f"self.text([':50'])..."
